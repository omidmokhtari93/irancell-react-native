import React, { useState } from 'react'
import { View, StyleSheet, Text, ActivityIndicator, Image, TextInput, TouchableOpacity } from 'react-native'


export const Login = props => {
    const [phone, setPhone] = useState('')
    const [loading, setLoading] = useState(false)
    const login = e => {
        if (!phone) {
            alert('شماره وارد شده معتبر نمی باشد')
            return
        }
        setLoading(true)
        fetch('https://jsonplaceholder.typicode.com/todos/1')
            .then(response => response.json())
            .then(json => {
                setLoading(false)
                props.navigation.navigate('LoginTypes', {
                    phone: phone,
                    email: 'omid****@**hoo.com'
                })
            })
    }

    return <View style={styles.layout}>
        <Image style={styles.irancellLogo} source={require('./irancell_new.png')} />
        <View>
            <Text style={styles.loaginTitle}>ورود</Text>
            <TextInput style={styles.phoneInput}
                onChangeText={setPhone}
                placeholder="شماره همراه/وایمکس/اینترنت ثابت"
            />
            {loading ? <ActivityIndicator size="large" color="#0000ff" style={{ height: 115 }} />
                : <TouchableOpacity style={styles.loginButton}
                    onPress={login}>
                    <Text style={styles.buttonTitle}>ورود</Text>
                </TouchableOpacity>}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 25 }}>
                <TouchableOpacity style={styles.pills}>
                    <Image style={styles.pillsImages} source={require('./network.png')} />
                    <Text style={styles.pillsTitle}>خرید آنلاین</Text>
                    <Text style={styles.pillsTitle}>بسته اینترنت</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.pills}>
                    <Image style={styles.pillsImages} source={require('./rules.png')} />
                    <Text style={styles.pillsTitle}>مشاهده قوانین</Text>
                    <Text style={styles.pillsTitle}>سرویس</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.pills}>
                    <Image style={styles.pillsImages} source={require('./code.png')} />
                    <Text style={styles.pillsTitle}>کد دستوری</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.pills}>
                    <Image style={styles.pillsImages} source={require('./simcard.png')} />
                    <Text style={styles.pillsTitle}>خرید آنلاین</Text>
                    <Text style={styles.pillsTitle}>سیم کارت</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.forgetArea}>
                <Image />
                <TouchableOpacity onPress={() => props.navigation.navigate('Forget', { name: 'Forget' })}>
                    <Text style={styles.forgetAreaText}>
                        شماره اینترنت ثابت را فراموش کرده ام
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    layout: {
        paddingHorizontal: 80,
        paddingVertical: 50,
        backgroundColor: '#e7e7e7',
        flex: 1
    },
    loaginTitle: {
        color: 'black',
        fontFamily: 'iransans',
        fontSize: 35,
        fontWeight: '800',
        marginBottom: 8,
        paddingRight: 20
    },
    irancellLogo: {
        width: 100,
        height: 100,
        marginBottom: 100
    }, phoneInput: {
        backgroundColor: 'white',
        height: 70,
        borderRadius: 5,
        fontFamily: 'iransans',
        paddingHorizontal: 20,
        fontSize: 20,
        marginBottom: 20
    }, loginButton: {
        backgroundColor: '#0180a3',
        paddingVertical: 10,
        marginBottom: 40
    }, buttonTitle: {
        color: 'white',
        textAlign: 'center',
        fontFamily: 'iransans',
        fontSize: 30
    }, pills: {
        alignItems: 'center',
        marginBottom: 50
    }, pillsImages: {
        width: 75,
        height: 75,
        marginBottom: 10
    }, pillsTitle: {
        fontFamily: 'iransans',
        color: 'black'
    }, forgetArea: {
        backgroundColor: '#fff1d6',
        marginHorizontal: 25,
        padding: 15,
        borderRadius: 10
    }, forgetAreaText: {
        fontFamily: 'iransans',
        color: '#50a2b5',
        borderBottomColor: '#50a2b5',
        borderBottomWidth: 1,
        alignSelf: 'flex-end'
    }
})